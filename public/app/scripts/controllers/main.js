'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the publicApp
 */
// angular.module('publicApp')
//   .controller('MainCtrl', function ($scope, Data) {

//     Data.getCars().success(parseCars);

// 	function parseCars(data) {
// 		$scope.cars = data;
// 		console.log($scope.cars);
// 	}

//     Data.getCar(id).success(parseCar);

// 	function parseCar(id) {
// 		$scope.vehicle = data;
// 		console.log($scope.vehicle);
// 	}

// 	$scope.newCar = { name: '', email: '' };

// 	$scope.addCar = function addCar() {
// 		var names = $scope.newCar.name.split(' ');
// 		Data.addCar({
// 			first_name: names[0],
// 			last_name: names[1],
// 			email: $scope.newCar.email
// 		})
// 		.success(carAddSuccess).error(carAddError);
// 	}

// 	function carAddSuccess(data) {
// 		$scope.error = null;
// 		$scope.cars.push(data);
// 		$scope.newCar = { name: '', email: '' };
// 	}

// 	function carAddError(data) {
// 		$scope.error = data;
// 	}

	// $scope.removeCar = function removeCar(id) {
	// 	if (confirm('Do you really want to remove this car?')) {
	// 		Data.removeCar(id).success(carRemoveSuccess);
	// 	}
	// }

	// function carRemoveSuccess(data) {
	// 	var i = $scope.cars.length;
	// 	while (i‐‐) {
	// 		if ($scope.cars[i].id == data) {
	// 			$scope.cars.splice(i, 1);
	// 		}
	// 	}
	// }

  //});

angular.module('publicApp')
  .controller('MainCtrl', function ($scope, Cars, $location, $routeParams, $rootScope) {

  	// Get all cars back
    $scope.cars = Cars.get();
    //console.log($scope.cars);

    // Get a car by id
    //$scope.carIdValue = 2;
    //console.log($scope.carIdValue);
    //$scope.vehicle = Cars.get( { id: '9' } );
    //$scope.vehicle = Cars.get( { id: $scope.carIdValue } );
    //$scope.vehicle = Cars.get( { id: id } );
    //$scope.vehicle = Vehicle.get();
    //console.log($scope.vehicle);

    // $scope.car = function() {
    //   Cars.get({
    //     carId: $routeParams.carId
    //   }, function(car) {
    //     $scope.car = car;
    //   });
    // };

    // $scope.findOne = function() {
    //   Cars.get({
    //     carId: $routeParams.carId
    //   }, function(car) {
    //     $scope.car = car;
    //   });
    // };

    $scope.create = function() {
      var car = new Cars({
        title: this.title,
        content: this.content
      });
      car.$save(function(response) {
        $location.path("cars/" + response._id);
      });

      this.title = "";
      this.content = "";
    };

    $scope.remove = function(car) {
      car.$remove();

      for (var i in $scope.cars) {
        if ($scope.cars[i] == car) {
          $scope.cars.splice(i, 1);
        }
      }
    };

    $scope.update = function() {
      var car = $scope.car;
      car.$update(function() {
        $location.path('cars/' + car._id);
      });
    };

  });
