<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Incrimental APIs | JM Try</title>
	<!-- <link rel="stylesheet" href="http://localhost/mysite.local/htdocs/progress-s-l4/styles/vendor.min.css">
	<link rel="stylesheet" href="http://localhost/mysite.local/htdocs/progress-s-l4/styles/site.min.css"> -->
</head>
<body>
	<div class="container">
		<div class="page-header">
			@yield('header')
		</div>
		
		@if(Session::has('message'))
			<div class="alert alert-success">
				{{Session::get('message')}}
			</div>
		@endif

		@yield('content')
	</div>
</body>
</html>