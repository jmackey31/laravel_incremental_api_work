<?php

class HomeController extends BaseController {

	public function showLogin()
	{
		if(Auth::check())
		{
			return Redirect::to('/');
		} 
		else {
			return View::make('login');
		}
	}

	public function doLogin()
	{
		$rules = array(
			'username'    => 'required',
			'password' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator)
				->withInput(Input::except('password'));
		} else {
			$userdata = array(
				'username' 	=> Input::get('username'),
				'password' 	=> Input::get('password')
			);

			if (Auth::attempt($userdata)) {
				Session::flash('message', 'You just logged in!');
				return Redirect::to('/');
			} else {
				Session::flash('message', 'No Match Buddy!');
				return Redirect::to('login');
			}

		}
	}

	public function create()
	{
		return View::make('registration.create');
	}

	public function store()
	{
		$input = Input::only('username', 'email', 'password', 'password_confirmation');

		$rules = [
		 'username' => 'required|unique:users',
		 'email' => 'required|email|unique:users',
		 'password' => 'required|confirmed'
		];

		$validator = Validator::make($input, $rules);

		if ($validator->fails())
		 return Redirect::back()->withInput()->withErrors($validator->messages());

		$user = User::create($input);

		Auth::login($user);

		return Redirect::route('/');
	}

	public function doLogout()
	{
		Auth::logout(); // log the user out of our application
		return Redirect::to('login'); // redirect the user to the login screen
	}

}
