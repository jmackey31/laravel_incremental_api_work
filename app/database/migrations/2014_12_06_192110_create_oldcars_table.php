<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldcarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Schema::create('cars', function(Blueprint $table)
		// {
		// 	$table->increments('id');

		// 	$table->string('car_make', 50);
		// 	$table->string('car_model', 50);
		// 	$table->string('vehicle_color', 50);
		// 	$table->string('dealership', 50);
		// 	$table->boolean('some_bool');

		// 	$table->timestamps();
		// });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cars', function(Blueprint $table)
		{
			Schema::drop('cars');
		});
	}

}
