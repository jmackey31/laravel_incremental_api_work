<?php 
namespace Acme\Transformers;

abstract class Transformer {

	/**
	 * Transform a collection of cars
	 *
	 * @param $cars
	 * @return array
	 */
	public function transformCollection(array $items)
	{
		return array_map([$this, 'transform'], $items);
	}

	public abstract function transform($item);

}