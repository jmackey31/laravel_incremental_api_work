<?php

class Events extends \Eloquent
{
	protected $fillable = ['title', 'starts', 'end', 'allDay'];

	protected $hidden = ['id'];
}