<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Schema::create('plants', function(Blueprint $table)
		// {
		// 	$table->increments('id');

		// 	$table->string('name', 50);
		// 	$table->string('leaf_color', 50);
		// 	$table->string('ideal_climate', 50);
		// 	$table->string('origin', 50);
		// 	$table->integer('cost');

		// 	$table->timestamps();
		// });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plants', function(Blueprint $table)
		{
			Schema::drop('plants');
		});
	}

}
