<?php 
namespace Acme\Transformers;

class EventTransformer extends Transformer {

	public function transform($event)
	{
		return [
			'id' => $event['id'],
			'title' => $event['title'],
			'start' => $event['start'],
			'end' => $event['end'],
			'allDay' => boolean) $event['allDay']
		];
	}
}