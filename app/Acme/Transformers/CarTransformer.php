<?php 
namespace Acme\Transformers;

class CarTransformer extends Transformer {

	public function transform($car)
	{
		return [
			'id' => $car['id'],
			'car_make' => $car['car_make'],
			'car_model' => $car['car_model'],
			'vehicle_color' => $car['vehicle_color'],
			'dealership' => $car['dealership'],
			'active' => (boolean) $car['some_bool']
		];
	}
}