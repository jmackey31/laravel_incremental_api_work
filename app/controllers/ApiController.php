<?php

class ApiController extends BaseController 
{

	const HTTP_NOT_FOUND = 404;

	/**
	 * @var int
	 */
	protected $statusCode = 200;

	/**
	 * @return mixed
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}

	/**
	 * @param mixed $statusCode
	 * @return $this
	 */
	public function setStatusCode($statusCode)
	{
		$this->statusCode = $statusCode;

		return $this;
	}

	/**
	 * @param string $message
	 * @return mixed
	 */
	public function respondNotFound($message = 'Not Found!')
	{
		// Manually setting the status code
		// return $this->setStatusCode(404)->respondWithError($message);

		// Using a global constant (see up top) to set the status code
		return $this->setStatusCode(self::HTTP_NOT_FOUND)->respondWithError($message);
	}

	public function respondInternalError($message = 'Internal Error!')
	{
		return $this->setStatusCode(500)->respondWithError($message);
	}

	/**
	 * @param $cars
	 * @param $data
	 * @return Response
	 */
	protected function respondWithPagination($cars, $data)
	{
		$data = array_merge($data, [
			'paginator' => [
				'total_count' => $cars->getTotal(),
				'total_pages' => ceil($cars->getTotal() / $cars->getPerPage()),
				'current_page' => $cars->getCurrentPage(),
				'limit' => $cars->getPerPage()
			]
		]);

		return $this->respond($data);
	}

	public function respond($data, $headers = [])
	{
		return Response::json($data, $this->getStatusCode(), $headers);
	}

	public function respondWithError($message)
	{
		return $this->respond([
			'error' => [
				'message' => $message,
				'status_code' => $this->getStatusCode()
			]
		]);
	}

	/**
	 * @param $message
	 * @return mixed
	 */

	protected function respondCreated($message)
	{
		return $this->setStatusCode(201)->respond([
			'message' => $message
			// 201 = created
		]);
	}

}