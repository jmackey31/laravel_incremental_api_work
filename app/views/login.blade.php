<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<title>Login | Incrimental APIs | JM Try</title>
</head>
<body class="container">
	<section id="main-content">
		<div id="main-content-login">
			<h2>Login</h2>

			<!-- will be used to show any messages -->
			@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
			@endif

			{{ Form::open(array('url' => 'login')) }}

				<!-- if there are login errors, show them here -->
				<p>
					{{ $errors->first('username') }}
					{{ $errors->first('password') }}
				</p>

				<p>
					{{ Form::text('username', Input::old('username'), array('placeholder' => 'username')) }}
				</p>

				<p>
					{{ Form::password('password', array('placeholder' => 'password')) }}
				</p>

				<p>{{ Form::submit('Submit!') }}</p>
			{{ Form::close() }}

			<ul>
				<li>
					<a href="#">forgot password?</a>
				</li>
				<li>
					<a href="{{ URL::to('/register') }}">
						Don't have an account?
						SIGN UP!
					</a>
				</li>
			</ul>
		</div>
	</section>
</body>
</html>