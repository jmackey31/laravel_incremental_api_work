<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<title>List of Cars and Plants</title>
	<meta name="description" content="List of Cars and Plants">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>
<body class="container" ng-app="publicApp">
	<!--[if lt IE 7]>
	  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<header id="main_header" class="row">
		<h1 class="col-xs-12 col-sm-4 col-md-3">List of Cars and Plants</h1>
		<!-- <a href="{{ URL::to('logout') }}">
			<button type="button" class="pull-right btn btn-danger btn-lg">Log Out</button>
		</a> -->
	</header>
	<section id="main-content" class="row">
		<nav id="main-content-side-nav" class="col-xs-12 col-md-2 col-lg-2">
			<ul class="nav nav-pills nav-stacked">
				<li class="active"><a ng-href="#/cars">Home</a></li>
				<li><a ng-href="#/about">About</a></li>
			</ul>
		</nav>
		<div id="main-content-inner" class="col-xs-12 col-md-10 col-lg-10">
			<div ng-view=""></div>
		</div>
	</section>
	<!-- _________________________________________ -->
	<footer id="footer-content" class="row">
		<p>Footer 2014</p>
	</footer>
	<!-- _________________________________________ -->

    <!-- build:js(.) scripts/oldieshim.js -->
    <!--[if lt IE 9]>
    <script src="bower_components/es5-shim/es5-shim.js"></script>
    <script src="bower_components/json3/lib/json3.js"></script>
    <![endif]-->
    <!-- endbuild -->

    <!-- build:js(.) scripts/vendor.js -->
    <!-- bower:js -->
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script src="bower_components/angular-animate/angular-animate.js"></script>
    <script src="bower_components/angular-cookies/angular-cookies.js"></script>
    <script src="bower_components/angular-resource/angular-resource.js"></script>
    <script src="bower_components/angular-route/angular-route.js"></script>
    <script src="bower_components/angular-sanitize/angular-sanitize.js"></script>
    <script src="bower_components/angular-touch/angular-touch.js"></script>
    <!-- endbower -->
    <!-- endbuild -->

        <!-- build:js({.tmp,app}) scripts/scripts.js -->
        <script src="app/scripts/app.js"></script>
        <script src="app/scripts/controllers/main.js"></script>
        <script src="app/scripts/controllers/about.js"></script>
        <script src="app/scripts/services/Cars.js"></script>
        <!-- endbuild -->
</body>
</html>