<?php

class Plants extends \Eloquent
{
	//Prevent Mass Assignment, so only show these
	protected $fillable = [
		'name',
		'leaf_color', 
		'ideal_climate',
		'origin',
		'cost'
	];

	//colums not listed
	// 'id'
	// 'created_at'
	// 'updated_at'

	//You can hide table elements from the api by using this
	//protected $hidden = ['vehicle_color'];
}