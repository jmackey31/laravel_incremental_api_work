<?php

Route::get('/', function()
{
	return View::make('yeah');
});

Route::get('/register', 'HomeController@create');
Route::resource('registration', 'RegistrationController');

Route::get('login', 'HomeController@showLogin');
Route::post('login', ['as' => 'login', 'uses' => 'HomeController@doLogin']);
Route::get('logout', 'HomeController@doLogout');

Route::group(['prefix' => 'api/v1'], function()
{
	Route::resource('cars', 'CarsController');
});

Route::group(['prefix' => 'api/v1'], function()
{
	Route::resource('plants', 'PlantsController');
});

Route::group(['prefix' => 'api/v1'], function()
{
	Route::resource('events', 'EventsController');
});

// Route::get('showallusers', function() {
// 	$allusers =  User::all();

// 	return $allusers;
// });