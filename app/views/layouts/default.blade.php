<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie-7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie-8 lt-ie9"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie-9 lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Progress S</title>
	<meta name="description" content="Project Management Software">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="favicon.ico">
	{{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.css') }}
	{{ HTML::script('scripts/modernizr.js') }}
</head>
<body class="container">
	<header id="main_header" class="row">
		<h1 class="col-xs-12 col-sm-4 col-md-3">Progress "S"</h1>
		<a href="{{ URL::to('logout') }}">
			<button type="button" class="pull-right btn btn-danger btn-lg">Log Out</button>
		</a>
	</header>
	<section id="main-content" class="row">
		<nav id="main-content-side-nav" class="col-xs-12 col-md-2 col-lg-2">
			<ul class="nav nav-pills nav-stacked">
				<li><a href="{{ URL::route('home') }}">Dashboard</a></li>
				<li><a href="{{ URL::route('projects.index') }}">All Projects</a></li>
				<li><a href="{{ URL::route('issues.index') }}">All Issues</a></li>
				<li><a href="{{ URL::route('admin.index') }}">Admin</a></li>
			</ul>
		</nav>
		<div id="main-content-inner" class="col-xs-12 col-md-10 col-lg-10">

		@yield('content')

	</section>
	<!-- _________________________________________ -->
	<footer id="footer-content" class="row">
		<p>Footer 2014</p>
	</footer>
	<!-- _________________________________________ -->
	{{ HTML::script('scripts/vendor.min.js') }}
	{{ HTML::script('scripts/main.min.js') }}
</body>
</html>