<?php

class Cars extends \Eloquent
{
	protected $fillable = ['car_make', 'car_model', 'vehicle_color', 'dealership'];

	//colums not listed
	// 'some_bool'
	// 'id'
	// 'created_at'
	// 'updated_at'

	//You can hide table elements from the api by using this
	//protected $hidden = ['vehicle_color'];
}