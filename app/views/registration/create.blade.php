<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
	<title>Register | Incrimental APIs | JM Try</title>
</head>
<body class="container">
	<section id="main-content">
		<div id="main-content-login">
			<h2>Register</h2>

			<!-- will be used to show any messages -->
			@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
			@endif

			{{ Form::open(['route' => 'registration.store']) }}

				<!-- if there are login errors, show them here -->
				<p>
					{{ $errors->first('username') }}
					{{ $errors->first('email') }}
					{{ $errors->first('password') }}
				</p>

				<p>
					{{ Form::text('username', Input::old('username'), array('placeholder' => 'username', 'required' => 'required')) }}
			        {{ $errors->first('username', '<span class="error">:message</span>') }}
				</p>

				<p>
					{{ Form::text('email', Input::old('email'), array('placeholder' => 'email', 'required' => 'required')) }}
			        {{ $errors->first('email', '<span class="error">:message</span>') }}
				</p>

				<p>
					{{ Form::text('password', Input::old('password'), array('placeholder' => 'password', 'required' => 'required')) }}
			        {{ $errors->first('password', '<span class="error">:message</span>') }}
				</p>

				<p>
					{{ Form::text('password_confirmation', Input::old('password_confirmation'), array('placeholder' => 'password_confirmation', 'required' => 'required')) }}
				</p>

				<p>
					{{ Form::submit('Create Account') }}
				</p>

			{{ Form::close() }}

			<ul>
				<li>
					<a href="#">forgot password?</a>
				</li>
				<li>
					<a href="{{ URL::to('/login') }}">
						Back to Login Page
					</a>
				</li>
			</ul>

		</div>
	</section>
</body>
</html>