<?php

use Acme\Transformers\CarTransformer;

class CarsController extends ApiController {

	/**
	 * @var Acme\Transformers\CarTransformer
	 */
	protected $carTransformer;

	function __construct(CarTransformer $carTransformer)
	{
		$this->carTransformer = $carTransformer;

		$this->beforeFilter('auth.basic', ['on' => 'post']);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//1. Querying all is bad
		//2. No way to attached meta data
		//3. Linking db structure to the API output
		//4. No way to signal headers/response codes
		//return Cars::all();

		// Paginate the query
		$limit = Input::get('limit') ?: 5;
		$cars = Cars::paginate($limit);

		// Die and dump class methods on paginator class
		//dd(get_class_methods($cars));

		return $this->respondWithPagination($cars, [
			'data' => $this->carTransformer->transformCollection($cars->all()),
		]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//Don't use the store method on a live site without SSL!
		if(!Input::get('car_make') or !Input::get('car_model'))
		{
			// return some kind of response
			// 400 = failed validation, 
			// 403 = forbidden, 
			// 422 = unprocessible intity
			return $this->setStatusCode(422)
				->respondWithError('Parameters failed validation for a Car.');
		}

		Cars::create(Input::all());

		return $this->respondCreated('Car successfully created.');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$car = Cars::find($id);

		if (!$car)
		{
			return $this->respondNotFound('Car does not exist.');
		}

		return $this->respond([
			'data' => $this->carTransformer->transform($car)
		]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//Don't use the store method on a live site without SSL!

		if(!Input::get('car_make') or !Input::get('car_model'))
		{
			//dd(Input::get('car_make'));
			return $this->setStatusCode(422)
				->respondWithError('Parameters failed update validation for a Car.');
		} else {
			// store
			$car = Cars::find($id);
			//dd($car);
			$car->car_make 		= Input::get('car_make');
			$car->car_model 	= Input::get('car_model');
			$car->vehicle_color 	= Input::get('vehicle_color');
			$car->dealership 	= Input::get('dealership');
			$car->active 		= Input::get('active');
			$car->save();

			return $this->respondCreated('Car successfully updated.');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
		$cars = Cars::find($id);
		// return $cars;
		// die();

		$cars->delete();

		return $this->respondCreated('Car successfully deleted.');
	}

}
