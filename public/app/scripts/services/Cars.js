'use strict';

// angular.module('publicApp')
//   .factory('Data', function Data($http) {
// 	return {
// 		getCars: function getCars() { return $http.get('/api/v1/cars'); },
// 		getCar: function getCar(id) { return $http.get('/api/v1/cars/'+ id); },
// 		//getCar: function getCar(id) { return $http.get('/api/v1/cars?id='+ id); },
// 		addCar: function addCar(data) { return $http.post('/api/v1/cars', data); },

// 		// THIS MAY NOT WORK BECUASE PDF WAS MISING THE END OF THIS LINE.
// 		// GOING TO TRY IT OUT ANYWAY
// 		removeCar: function removeCar(id) { return $http.delete('/api/v1/cars/'+ id); }
// 	}
// });

angular.module('publicApp')
  .factory('Cars', function ($resource) {
    return $resource('/api/v1/cars/:id', { id: '@id' } );
});

// angular.module('publicApp')
//   .factory('Vehicle', function Vehicle($http) {
// 	return {
// 		getVehicle: function getVehicler(id) { return $http.get('/api/v1/cars/'+ id); }
// 	}
// });

// angular.module('publicApp')
//   .factory('Cars', function ($resource) {
//     return $resource('/api/v1/cars/:carId', {
//       carId: '@_id'
//     }, {
//       update: {
//         method: 'PUT'
//       }
//     });
// });
