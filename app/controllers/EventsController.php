<?php

class EventsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$events = Events::all();

		return Response::json([
			'data' => $events->toArray()
		], 200);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$event = Events::find($id);

		if (!$event)
		{
			return Response::json([
				'error' => [
					'message' => 'Event does not exist'
				]
			], 404);
		}

		return Response::json([
			'data' => $event->toArray()
		], 200);
	}

}
