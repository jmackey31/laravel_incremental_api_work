<?php

class PlantsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//1. Querying all is bad
		//2. No way to attached meta data
		//3. Linking db structure to the API output
		//4. No way to signal headers/response codes
		//return Plants::all();

		$plants = Plants::all();

		// Old way of returning data
		// return Response::json([
		// 	'data' => $plants->toArray()
		// ], 200);

		return Response::json([
			'data' => $this->transformCollection($plants)
		], 200);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$plant = Plants::find($id);

		if (!$plant)
		{
			return Response::json([
				'error' => [
					'message' => 'Plant does not exist'
				]
			], 404);
		}

		return Response::json([
			'data' => $this->transform($plant->toArray())
		], 200);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	private function transformCollection($plants)
	{
		return array_map([$this, 'transform'], $plants->toArray());
	}

	private function transform($plant)
	{
		return [
			'name' => $plant['name'], 
			'leaf_color' => $plant['leaf_color'], 
			'ideal_climate' => $plant['ideal_climate'],
			'origin' => $plant['origin'],
			'cost' => $plant['cost']
		];
	}

}
